function saveData(mongoDb, collectionName, data, callback) {
    try{
        mongoDb.open(function(err, db) {
            if (err) {
                mongoDb.close();
                console.log('An error has occurred in function saveData ,msg is ', err);
                return callback(constants.DBCONNECTFAIL);
            }
            db.collection(collectionName, function(err, collection) {
                if (err) {
                    console.log('An error has occurred in function saveData,msg is ', err);
                    mongoDb.close();
                    return callback(constants.DBCOLLECTIONFAIL);
                }
                collection.insert(data, function(err, data) {
                    mongoDb.close();
                    if (err) {
                        console.log('An error has occurred in function saveData,msg is ', err);
                        return callback(constants.DBWRITEINFAIL);
                    }
                    return callback(constants.SAVESUCESS);
                })
            })
        })
    }
    catch(e){
        return callback(constants.SERVEREXCEPTION);
    }
}

function getData(mongoDb, collectionName, username, callback) {
    try{
        mongoDb.open(function(err, db) {
            if (err) {
                mongoDb.close();
                console.log('An error has occurred in function getData,msg is ', err);
                return callback(constants.DBCONNECTFAIL, null);
            }
            db.collection(collectionName, function(err, collection) {
                if (err) {
                    console.log('An error has occurred in function getData,msg is ', err);
                    mongoDb.close();
                    return callback(constants.DBCONNECTFAIL, null);
                }
                collection.findOne({ username: username }, function(err, user) {
                    mongoDb.close();
                    if (err) {
                        console.log('An error has occurred in function getData,msg is ', err);
                        return callback(constants.DBCONNECTFAIL, null);
                    }
                    return callback(constants.DBGETUSERSUCCESS, user);
                })
            })
        })
    }
    catch(e){
        return callback(constants.SERVEREXCEPTION);
    }
}

function userIsExist(mongoDb, collectionName, username, callback){
    try{
        mongoDb.open(function(err, db) {
            if (err) {
                console.log('An error has occurred in function userIsExist,msg is ',err);
                return callback(err,constants.QUERYFAIL);
            }
            db.collection(collectionName, function(err, collection) {
                if (err) {
                    mongoDb.close();
                    console.log('An error has occurred in function userIsExist,msg is ',err);
                    return callback(err,constants.QUERYFAIL);
                }
                collection.findOne({ username: username }, function(err, user) {
                    mongoDb.close();
                    if (err) {
                        console.log('An error has occurred in function userIsExist,msg is ',err);
                        return callback(err,constants.QUERYFAIL);
                    }
                    if(user!==null&&user!==undefined){
                        return callback('exist',constants.USERISEXIST);
                    }
                    else{
                        return callback(null,constants.UNEXISTEDNAME);
                    }
                })
            })
        })
    }
    catch(e){
        return callback(e,constants.SERVEREXCEPTION);
    }
}


function getList(mongoDb,collectionName,data, callback) {
    try{
        mongoDb.open(function(err, db) {
            if (err) {
                mongoDb.close();
                console.log('An error has occurred in function getList,msg is ',err);
                return callback(err,constants.DBCONNECTFAIL);
            }
            db.collection(collectionName, function(err, collection) {
                if (err) {
                    mongoDb.close();
                    console.log('An error has occurred in function getList,msg is ',err);
                    return callback(err,constants.DBCONNECTFAIL);
                }
                collection.count({}, function(err, count) {
                    collection.find({}, {
                        limit: parseInt(data.pageSize),
                        skip: (parseInt(data.currentPage) - 1) * parseInt(data.pageSize)
                    }).sort({
                        date: -1
                    }).toArray(function(err, list) {
                        mongoDb.close();
                        var page = {};
                        page["count"] = count;
                        page["pageSize"] = data.pageSize;
                        page["currentPage"] = data.currentPage;
                        return callback(null, list, page);
                    })
                });
            })
        })
    }
    catch(e){
        return callback(e,constants.SERVEREXCEPTION);
    }
}
