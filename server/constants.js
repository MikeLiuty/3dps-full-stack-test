//This file is saving all the error massages
const SAVESUCESS = {code:'1001',msg:'You posted a new post!'}
const FINDNOTHING = {code:'1002',msg:'Find Nothing'}
const DBCONNECTFAIL ={code:'003',msg:'Failed to connect Database'}
const DBCOLLECTIONFAIL ={code:'004',msg:'Connect to collection failed'}
const DBWRITEINFAIL ={code:'005',msg:'Database write failed'}
const SAVEFAIL = {code:'006',msg:'Fail saved failed'}
const DBUNKNOWNERROR ={code:'007',msg:'Unknow error'}
const SERVEREXCEPTION ={code:'008',msg:'Cannot connect to server'}

module.exports = {
  SAVESUCESS,
  FINDNOTHING,
  DBCONNECTFAIL,
  DBCOLLECTIONFAIL,
  DBWRITEINFAIL,
  SAVEFAIL,
  DBUNKNOWNERROR,
  SERVEREXCEPTION
}
