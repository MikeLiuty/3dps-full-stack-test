import React, {Component} from 'react'
import {render} from 'react-dom'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import * as actions from '../actions/index'
import {Icon} from 'antd'; //import AntUI
import VoteBtn from './Vote.js';
import {Link} from 'react-router-dom'

//Display all the data on the home page
class Poster extends Component {
  constructor(props) {
    super(props);
    this.state = {
      _id: this.props.posterInfo._id,
      author: this.props.posterInfo.author,
      title: this.props.posterInfo.title,
      content: this.props.posterInfo.content,
      date: this.props.posterInfo.date,
      votes: this.props.posterInfo.votes
    };
    console.log('PostList. Function Poster of cosnt is ' + this.state);
  }
  componentWillReceiveProps(nextProps) {
    this.setState({
      _id: this.props.posterInfo._id,
      author: this.props.posterInfo.author,
      title: this.props.posterInfo.title,
      content: this.props.posterInfo.content,
      date: this.props.posterInfo.date,
      votes: this.props.posterInfo.votes
    });
  console.log('PostList. Function Poster will recieve ' + this.state);
  }
  // componentDidMount() {
  //   console.log('votes' + this.props.posterInfo.votes);
  // }
  render() {
    let content;
    let showDate = new Date(this.state.date).toLocaleString();
    content = this.state.content;
    return (<div className='post'>
      <div className='title'>
        Title: {this.state.title}
      </div>
      <div className='author'>
        Author: {this.state.author}
      </div>
      <div className='content'>
        Post: {content}
      </div>
      <VoteBtn/>
      : {this.state.votes}
      <div className='date'>
        Date: {showDate}
      </div>
    </div>)
  }
}

class PostList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posterInfo: this.props.posterInfo
    }
    console.log('PostList. Function PostList of cosnt posterinfo is ' + this.state );
  }
  componentWillReceiveProps(nextProps) {
    this.setState({posterInfo: nextProps.posterInfo});
      console.log('PostList. Function PostList componentWillReceiveProps of cosnt posterinfo is ' + this.state );
  }
  //Display the poster component
  render() {
    var content = new Array();
    for (var i = 0; i < this.state.posterInfo.length; ++i) {
      content.push(<Poster key={this.state.posterInfo[i]._id} posterInfo={this.state.posterInfo[i]}></Poster>)
    }
    return (<div>{content}</div>)
  }
}

export default PostList;
