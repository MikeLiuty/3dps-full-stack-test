import { CHANGEPAGENO,CHANGEPAGESIZE,CHANGEPAGETOTAL } from '../constants'

//The code will be used when the page turning setting changes
function changePage(state = {currentPage:1,total:10,pageSize:10}, action){
	console.log('page in reducers,state is ',state,'action is ',action);
  if(action.type === CHANGEPAGENO) {
    return Object.assign({},state,{currentPage: action.data});
  }
  else if(action.type === CHANGEPAGESIZE){
    return Object.assign({},state,{pageSize: action.data});
  }
  else if(action.type === CHANGEPAGETOTAL){
    return Object.assign({},state,{total: action.data});
  }
  return state;
}

export default changePage;
