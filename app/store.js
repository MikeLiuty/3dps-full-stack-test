
import { createStore,combineReducers,applyMiddleware,compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import {routerReducer, routerMiddleware} from 'react-router-redux'
import * as reducers from './reducers/index'

import DevTools from './containers/DevTools'

function myCreateStores(history){
	const middleware = routerMiddleware(history);
	const enhancer = compose(
	  applyMiddleware(thunkMiddleware,middleware),
	  DevTools.instrument()
	)
	const store = createStore(
	    combineReducers({
	    	...reducers,
	    	routing: routerReducer
	    }),enhancer
	)
	console.log('store ' +store);
	return store;
}

export default myCreateStores
