const SERVERPORT ='8001';

var express = require('express');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser')
var session = require('express-session')
var mongoConnect = require('connect-mongo')(session)
var Server = require('mongodb').Server
var Db = require('mongodb').Db
var mongoDb = new Db('HackerNews', new Server('localhost', 27017, { safe: true }));

var Post = require('./entities/Post')
var post_get = require('./post_get/index');

var constants = require('./constants');

require('events').EventEmitter.prototype._maxListeners = 100;

var app = express();
app.use(cookieParser())
app.use(session({
    secret: 'HackerNews',
    key: 'HackerNews',
    resave: false,
    saveUninitialized: true,
    cookie: { maxAge: 7200000,secure: false},

    store: new mongoConnect({
        url: 'mongodb://localhost/HackerNews'
    })
}))

app.use(bodyParser.urlencoded({ limit:'5mb',extended: true }));

app.all('*',function (req, res, next) {
  res.header('Access-Control-Allow-Origin', 'http://localhost:7000');
  res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, UPDATE, DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type=application/json;charset=UTF-8');
  res.header('Access-Control-Allow-Credentials', true)

  if (req.method == 'OPTIONS') {
    console.log('server/index option');

  }
  else {
    next();
  }
});

app.post('/register', function (req, res) {
  const username=req.body.username;
  const password=req.body.password;

  post_get.userIsExist(mongoDb,'users',username,function(err,result){
    if(err)
      res.send(JSON.stringify({ result })).end();
    else{
      var user = new User(username,password);
      post_get.saveData(mongoDb,'users',user,function(result){
        res.send(JSON.stringify({ result })).end();
      });
    }
  });
})

app.post('/login', function (req, res) {
  const username=req.body.username;
  const password=req.body.password;

  var user = new User(username,password);
  post_get.getData(mongoDb,'users',username,function(result,user){
    if(result.code === '1002'){
      if(user){
        if(password === user.password){
          var result = constants.LOGINSUCCESS;
          req.session.user = user
          result['userInfo']=user;
          res.send(JSON.stringify(result)).end();
        }
        else
          res.send(JSON.stringify(constants.WRONGPASSWORD)).end();
        }
      else
        res.send(JSON.stringify(constants.UNEXISTEDNAME)).end();
    }
    else
      res.send(JSON.stringify({ code: result.code, messgage: result.msg})).end();
  });
})


app.post('/checkIsLogin', function (req, res) {
  if (req.session.user) {
      var result = constants.USERALREADYLOGIN;
      result['userInfo'] = req.session.user;
      return res.json(result);
    } else {
       // return res.json({ code: 1001, messgage: "未登录" })
       //console.log('req.session.user is null');
       return res.json(constants.USERNOTLOGIN);
    }
})

app.post('/logOut', function (req, res) {
  req.session.user = null;
  return res.json(constants.USERALREADYLOGOUT);
})


app.post('/recordPost',function (req, res) {
  var post = new Post(req.body.title,req.body.content,req.body.author);
  post_get.saveData(mongoDb,'posts',post,function(result){
     res.send(JSON.stringify({ result })).end();
      console.log('server/index post/recordpost');
  });
})

app.post('/getList',function(req,res){
  var currentPage = req.body.currentPage;
  var pageSize = req.body.pageSize;
  var data = {'currentPage':currentPage,'pageSize':pageSize};
  post_get.getList(mongoDb,'posts',data,function(err, list, page){
    var result = {};
    result["data"] = list;
    result["page"] = page;
    res.send(JSON.stringify({ result })).end();
  console.log('server/index post/getlist');
  });
})

var server = app.listen(SERVERPORT, function () {
  var host = server.address().address
  var port = server.address().port

})
