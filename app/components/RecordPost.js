import React, {Component} from "react"
import {render} from 'react-dom'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import * as actions from '../actions/index'
import '../static/scss/recordPost.scss';
import Login from './Login';
//The post submit form
import {
  Input,
  Form,
  Select,
  Button,
  Upload,
  Icon,
  message
} from 'antd';
const FormItem = Form.Item;
const Option = Select.Option;

class RecordPost extends React.Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  componentDidMount() {}

  handleSubmit(e) {
    e.preventDefault();
    const that = this;
    this.props.form.validateFields((err, values) => {
      if (err) {
        return;
      }

      var data = 'title=' + values.title + '&content=' + values.content + '&author=' + localStorage.author
      console.log('RecordPost inputted data is ' + data)
      that.props.actions.SubmitData('recordPost', data, that);
    });
  }
  render() {
    const {getFieldDecorator} = this.props.form;
    const formItemLayout = {
      labelCol: {
        span: 3
      },
      wrapperCol: {
        span: 18
      }
    };
    return (<div className='blog_post'>
      <Form onSubmit={this.handleSubmit}>
        {/* <FormItem {...formItemLayout} label={(<span>
            Username
          </span>)}> */}
          {/* {
            getFieldDecorator('author', {
              rules: [
                {
                  required: true,
                  message: 'Please input username!'
                }
              ]
            })(<Input/>)
          } */}
        {/* </FormItem> */}

        <FormItem {...formItemLayout} label={(<span>
            Title
          </span>)}>
          {
            getFieldDecorator('title', {
              rules: [
                {
                  required: true,
                  message: 'Please input title!'
                }
              ]
            })(<Input/>)
          }
        </FormItem>

        <FormItem {...formItemLayout} label={(<span>
            Please input your description
          </span>)}>
          {
            getFieldDecorator('content', {
              rules: [
                {
                  required: true,
                  message: 'Please input your post!'
                }
              ]
            })(<Input type='textarea' style={{
                minHeight: '300px'
              }}/>)
          }
        </FormItem>
        <FormItem wrapperCol={{
            offset: 19
          }}>
          <Button type='primary' htmlType='submit'>Submit</Button>
        </FormItem>
      </Form>
    </div>);
  }
}

RecordPost = Form.create({})(RecordPost);

const mapStateToProps = (state) => {
  return {userInfo: state.stores.userInfo}
  console.log('recordpost the state is ' + state.stores.userInfo);
}
const mapDispatchToProps = (dispatch) => {
  console.log('recordpost mapDispatchToProps');
  return {
    actions: bindActionCreators(actions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RecordPost)
