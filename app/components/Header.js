import React,{Component} from "react"
import {render} from 'react-dom'
import {connect} from 'react-redux'
import { bindActionCreators } from 'redux'
import * as actions from '../actions/index'

import {Link} from 'react-router-dom'
import {Layout, Menu, Icon} from 'antd'; //import AntUI]
const { Header } = Layout;

import '../static/css/header.css'

class PostHeader extends Component {
  constructor(props) {
    super(props);
    this.onSelect = this.onSelect.bind(this);
    this.login = this.login.bind(this);
    this.showLogin = this.showLogin.bind(this);
    this.showRegister = this.showRegister.bind(this);
    this.logout = this.logout.bind(this);
  }

  componentDidMount(){
    this.props.actions.CheckIsLogin();
  }

  onSelect(e) {
      this.props.actions.ChangeCurrentPage(e.key);
    }

  login(){
  	//console.log('login:',this.props);
  	this.props.actions.UpdateLoginState('login');
  }

  logout(){
    this.props.actions.LogOut();
  }
  showLogin(){
    this.props.actions.ShowModal('login','用户登录');
  }
  showRegister(){
    this.props.actions.ShowModal('register','用户注册');
  }

  render() {
  	let isLogin;
    let isRegister;
  	if(this.props.header.isLogin){
      if(this.props.userInfo.userInfo){
  		  isLogin = <div className='login'>欢迎您，{this.props.userInfo.userInfo.username}<a href='#' className ='logout' onClick={this.logout}>登出</a></div>
        isRegister = null;}
  	}
    else
  		{
        isLogin =<a className='label_login' onClick={this.showLogin}>Login</a>;
        isRegister=<a className='label_register' onClick={this.showRegister}>Register</a>}
  	//console.log('Header.js this.props is ',this.props)
    return (
      <Layout className='layout'>
	        <Header className='header'>
	        	<div className='title'>
              <div className='logo'>
                Mike's Hacker News
              </div>

            <Menu mode="horizontal" onSelect={this.onSelect} defaultSelectedKeys={['1']} className="buttons">
              <Menu.Item key="1">
                <Link to="/">Home Page</Link>
              </Menu.Item>
              <Menu.Item key="2">
                <Link to="/add">Submit</Link>
              </Menu.Item>
              <Menu.Item key="3">
                {isLogin}
              </Menu.Item>
              <Menu.Item key="4">
                {isRegister}
              </Menu.Item>

            </Menu>
            </div>
          </Header>
        </Layout>
      );
    }
  }

const mapStateToProps = (state) => {
  //console.log('Header.js mapStateToProps,state is ',state);
    return {header:state.stores.header,userInfo:state.stores.userInfo}
}
const mapDispatchToProps = (dispatch) => {
  //console.log('mapDispatchToProps');
    return {
        actions: bindActionCreators(actions, dispatch),
    }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PostHeader)
