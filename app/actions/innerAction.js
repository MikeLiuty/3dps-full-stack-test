import {SERVERADDRESS}from '../constants'
import { push } from 'react-router-redux'
import { message } from 'antd';

export function sendLoginInfo(path,dispatch,data,callback){
    fetch(SERVERADDRESS + '/' + path, {
            method: 'POST',
            mode: 'cors',
            credentials: 'include',
            headers: {
                 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
            body:  data
        }).then(function(res) {
            return res.json();
        }).then(function(result) {
            //console.log('result is ',result);
            if(result.code==='1003'){
                //console.log(result.messgage);
                dispatch(callback[0]());
                dispatch(callback[1](result.userInfo));
                dispatch(callback[2](path));

                message.success(result.msg);
            }
            else{
                console.log('Login Failed:',result.msg);
                message.error('Login Failed:'+result.msg);
            }
        })
        .catch(function(e) {
            console.error('expection is ',e);
            message.error("something wrong during login");
        });
}

export function sendRegisterInfo(path,dispatch, data,callback){
	fetch(SERVERADDRESS + '/' + path, {
            method: 'POST',
            mode: 'cors',
            credentials: 'include',
            headers: {
                 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
            body:  data
        }).then(function(res) {
            //console.log('res is ',res);
            return res.json();
        }).then(function(result) {
        	//console.log('result is ',result);
            result = result.result;
            if(result.code === '1004'||result.code === '1007'){
                console.log(result.msg);
                dispatch(callback());
                message.success(result.msg);
            }
            else if(result.code === '1010'){
                console.log('Username is taken');
                message.error('Username is taken,'+result.msg);
            }
            else{
                console.log('registration Failed',result.msg);
                message.error('registration Failed,'+result.msg);
            }
        })
        .catch(function(e) {
            console.error('expection is ',e);
            message.error('Something wrong during registration');
        });
}

export function checkIsLogin(path,dispatch,callback,key){
    fetch(SERVERADDRESS + '/' + path, {
            method: 'POST',
            mode: 'cors',
            credentials: 'include',
            headers: {
                 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            }
        }).then(function(res) {
            return res.json();
        }).then(function(result) {
            //console.log('result is ',result);
            if(result.code ==='1005'){
                dispatch(callback[0](key));
                dispatch(callback[1](result.userInfo));
                //console.log(result.code,result.code);
            }
            else if(result.code === '0015'){
                console.log(result.msg);
                //console.log(result.code,result.code);
            }
            else{
                console.log('Unknow error');
            }
        })
        .catch(function(e) {
            console.log('expection is ',e);

        });
}

export function logOut(path,dispatch,callback){
    fetch(SERVERADDRESS + '/' + path, {
            method: 'POST',
            mode: 'cors',
            credentials: 'include',
            headers: {
                 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            }
        }).then(function(res) {
            return res.json();
        }).then(function(result) {
            //console.log('result is ',result);
            if(result.code === '1006' ){
                dispatch(callback[0](path));
                dispatch(callback[1](null));
            }
        })
        .catch(function(e) {
            console.log('expection is ',e);
            message.error('Logout Failed');
        });
}


//Save all the data to Database
export function recordPost(path,dispatch,data,source,callback){
  console.log('actions/innerAction recordpost:'+data);
    fetch(SERVERADDRESS + '/' + path, {
            method: 'POST',
            mode: 'cors',
            credentials: 'include',
            headers: {
                 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
            body:data
        }).then(function(res) {
            return res.json();
        }).then(function(result) {
            console.log('result is ',result);
            result = result.result;
//add if statement for checking in the future
               source.props.history.push("/");
               dispatch(callback('1'));
               message.success(result.msg);
            })
        .catch(function(e) {
            console.log('expection is ',e);
            message.error('Errors during saving process');
        });
}
//display all the data from DB
export function getList(path,dispatch,callback,data,type){
    console.log('Actions/innerAction Getlist is ',data);
    fetch(SERVERADDRESS + '/' + path, {
            method: 'POST',
            mode: 'cors',
            credentials: 'include',
            headers: {
                 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
            body:data
        }).then(function(res) {
            return res.json();
        }).then(function(result) {
            //console.log('result is ',result);
            var result = result.result;
            if(type === 'initializePoster'){
                dispatch(callback[1](result.data));
                dispatch(callback[0](result.page.count));
            }
            else if(type === 'pageSizeChange'){
                dispatch(callback[1](result.data));
                dispatch(callback[0](result.page.pageSize));
            }
            else if(type === 'pageNoChange'){
                dispatch(callback[1](result.data));
                dispatch(callback[0](result.page.currentPage));
            }
        })
        .catch(function(e) {
            console.log('expection is ',e);
        });
          console.log('innerAction is ',data);
}
