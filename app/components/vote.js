import React,{Component} from "react"
import {render} from 'react-dom'
import {connect} from 'react-redux'
import { bindActionCreators } from 'redux'
import * as actions from '../actions/index'
import {Icon} from 'antd';//import AntUI
import '../static/scss/recordPost.scss';

//Able to let user vote the post they like. Will be fixed in the future.
class VoteBtn extends Component {
  render() {
    return (
<Icon type="caret-up" style={{ fontSize: 16, color: '#08c' }} className="likeBtn"/>
)};
}
export default VoteBtn;
