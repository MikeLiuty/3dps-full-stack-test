import {combineReducers} from "redux"
import changePage from './page'
import updatePosterInfo from './main'
import changeLoginState from './headers'
import switchModal from './modalDialog'
import updateUserInfo from './userInfo'

//When page turn the post will be changed as well
export const stores = combineReducers({
  header:changeLoginState,
  modalDialog:switchModal,
  userInfo:updateUserInfo,
  page:changePage,
  posterInfo:updatePosterInfo,
})
