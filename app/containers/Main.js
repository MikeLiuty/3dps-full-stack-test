import React, {Component} from "react"
import {render} from 'react-dom'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import * as actions from '../actions/index'
import {Layout} from 'antd';
import PostList from '../components/PostList';
import Page from '../components/Page';
import '../static/scss/main.scss';

class Main extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    console.log('main componentDidMount');
    var data = 'currentPage=1&pageSize=10';
    this.props.actions.GetList(data, 'initializePoster');
  }
  render() {
    console.log('mainjs this.props.posterInfo is ', this.props.posterInfo);
    if (this.props.posterInfo.posterInfo !== null) {
      console.log('Mainjs posterinfo is'+this.props.posterInfo);
      if (this.props.posterInfo.posterInfo.length === 0) {
        return (
          //PlaceHolder Texts
          <div className='main'>
            <div className='post'>
              <div className='title'>
                Title: PlaceHolder
              </div>
              <div className='author'>
                Author: Mike
              </div>
              <div className='content'>
                Post: This is just a placeholder
              </div>
              <div className='date'>
                Date of Post:07/04/2018
              </div>
            </div>
            <div className='post'>
              <div className='title'>
                Title: The 2nd PlaceHolder
              </div>
              <div className='author'>
                Author: Mike
              </div>
              <div className='content'>
                Post: This is just another placeholder
              </div>
              <div className='date'>
                Date of Post:07/04/2018
              </div>
            </div>
        </div>);
      } else if (this.props.posterInfo.posterInfo.length <= 8){
        return (
          <div className='main'>
          <PostList posterInfo={this.props.posterInfo.posterInfo}></PostList>
          <div className='post'>
            <div className='title'>
              Title: PlaceHolder
            </div>
            <div className='author'>
              Author: Mike
            </div>
            <div className='content'>
              Post: This is just a placeholder
            </div>
            <div className='date'>
              Date of Post:07/04/2018
            </div>
          </div>
          <div className='post'>
            <div className='title'>
              Title: The 2nd PlaceHolder
            </div>
            <div className='author'>
              Author: Mike
            </div>
            <div className='content'>
              Post: This is just another placeholder
            </div>
            <div className='date'>
              Date of Post:07/04/2018
            </div>
          </div>
          <Page></Page>
        </div>
      );
      }

      else{
        return (
          <div className='main'>
          <PostList posterInfo={this.props.posterInfo.posterInfo}></PostList>
          <Page></Page>
          </div>
      );
      }
    } else {
      return (<div className='not_found'>
        Something goes wrong here
      </div>);
    }
  }
}

const mapStateToProps = (state) => {
  console.log('Main.js mapStateToProps,state is ',state.stores.posterInfo);
  return {posterInfo: state.stores.posterInfo}
}
const mapDispatchToProps = (dispatch) => {
  console.log('Main.js mapDispatchToProps');
  return {
    actions: bindActionCreators(actions, dispatch)
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Main)
