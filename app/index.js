//import 'fetch-ie8';
import 'es5-shim';
import 'es5-shim/es5-sham';
import 'es6-shim';
import React,{Component} from 'react'
import ReactDOM from 'react-dom'
import { createStore, combineReducers,applyMiddleware} from 'redux'
import { Provider } from 'react-redux'
import createHistory from 'history/createBrowserHistory'
import { HashRouter as Router,Route,Link,Switch} from 'react-router-dom' //this will occur a warning but can't remove it.
import {syncHistoryWithStore} from 'react-router-redux'
import  * as reducers from './reducers/index'
import RecordPost from 'components/RecordPost'
import Login from 'components/Login'
import Main from 'containers/Main'
import DevTools from './containers/DevTools'
import myCreateStores from './store'
const browserHistory = createHistory()
import { CSSTransitionGroup } from 'react-transition-group'
import { Layout } from 'antd'
const { Content, Footer } = Layout

import Header from './components/Header'
import ModalDialog from './containers/ModalDialog'

import 'static/css/antd.css'
import './static/scss/index.scss'
const stores = myCreateStores(browserHistory)
const history = syncHistoryWithStore(browserHistory, stores)

//The main page to display all the content
ReactDOM.render(
  <Provider store={stores}>
    <Router history={history}>
      <Layout>
        <Header>
        </Header>
            <Content style={{minHeight:600}}>
            <CSSTransitionGroup transitionName='fade' transitionEnterTimeout={300} transitionLeaveTimeout={300}>
              <Switch>
                <Route exact path="/" location={history.location} key={history.location.key} component={Main}/>
                <Route path="/add" location={history.location} key={history.location.key} component={RecordPost}/>
                <Route path="/login" location={history.location} key={history.location.key} component={Login}/>
              </Switch>
            </CSSTransitionGroup>
          </Content>
        <Footer className='footer' style={{ textAlign: 'center',color:'white' }}>
          Footer
        </Footer>
       {/* <DevTools /> */}
      </Layout>
    </Router>
  </Provider>,
  document.getElementById('app')
)
