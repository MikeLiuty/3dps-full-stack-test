import React, {Component} from 'react'
import {render} from 'react-dom'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import * as actions from '../actions/index'
import {Pagination} from 'antd';

//Page Turning feature
class Page extends Component {
  constructor(props) {
    super(props);
    this.onShowSizeChange = this.onShowSizeChange.bind(this);
    this.onChange = this.onChange.bind(this);
  }
  componentDidMount() {
    // console.log('Page componentDidMount');
  }
  onShowSizeChange(current, pageSize) {
    let data = 'currentPage=' + current + '&pageSize=' + pageSize;
    this.props.actions.GetList(data, 'pageSizeChange');
  }
  onChange(page, pageSize) {
    // console.log('onChange,', page, pageSize);
    let data = 'currentPage=' + page + '&pageSize=' + pageSize;
    this.props.actions.GetList(data, 'pageNoChange');
  }
  render() {
    return (<div>
      <Pagination showSizeChanger={true} onShowSizeChange={this.onShowSizeChange} onChange={this.onChange} defaultCurrent={1} defaultPageSize={10} total={this.props.page.total} showTotal={total => `Total ${total} posts`} pageSizeOptions={['10', '20']}/>
    </div>);
  }
}

const mapStateToProps = (state) => {
  console.log('Page.js mapStateToProps,state is ' + state.stores.page);
  return {page: state.stores.page}
}
const mapDispatchToProps = (dispatch) => {
  console.log('Page.js mapDispatchToProps');
  return {
    actions: bindActionCreators(actions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Page)
