var path = require('path');
var webpack = require('webpack')
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var CleanPlugin = require('clean-webpack-plugin');
var CommonsChunkPlugin = webpack.optimize.CommonsChunkPlugin;
var prod = process.env.NODE_ENV === 'production' ? true : false;

module.exports = {
    entry: { index: './app/index.js',
        vendor: ['react', 'react-dom', 'react-router','react-router-dom']
    },
    output: {
        path: path.resolve(__dirname, prod ? "./dist" : "./build"),
        filename: prod ? "js/[name].[hash].min.js" : "js/[name].js",
        chunkFilename: prod ? "js/[name].[hash].chunk.js" : "js/[name].js",
        publicPath: prod ? "" : ""
    },
    resolve: {
        extensions: ['', '.js', '.scss', '.css', '.png', '.jpg'],
        root: path.resolve('./app'),
    },
    module: {
        loaders: [{
            test: /\.jsx?$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
            query: {
              presets: [["es2015", { "loose": true }],'react','stage-0'],

            }
        }, {
            test: /\.css$/,
            exclude: /node_modules/,
            loader: 'style-loader!css-loader'
        }, {
            test: /\.(png|jpg|jpeg|gif)$/,
            exclude: /node_modules/,
            loader: 'url?limit=10000&name=img/[name].[hash].[ext]'
        },
        {
            test: /\.scss$/,
            loader: ExtractTextPlugin.extract("style", 'css!sass')
        },
        {

            test: /\.(woff|woff2|svg|eot|ttf)\??.*$/,
            loader: 'url?name=./[name].[ext]',
        }]

    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: './index.html'
        }),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV)
        }),
        new ExtractTextPlugin("style.css"),
        new webpack.optimize.CommonsChunkPlugin('vendor',  'vendor.js'),
    ],
    devServer: {
        port: 7000,
        hot: true,
        historyApiFallback: true,
        publicPath: "",
        stats: {
            colors: true
        },
        plugins: [
            new webpack.HotModuleReplacementPlugin()
        ],
        inline:true,
    }
}
if (prod) {
    module.exports.plugins = (module.exports.plugins || [])
        .concat([
            new webpack.optimize.UglifyJsPlugin({
                output: {
                    comments: false,
                },
                compress: {
                    warnings: false,
                    drop_console: true
                }
            }),
            new webpack.optimize.OccurenceOrderPlugin(),
            new ExtractTextPlugin('[name].[hash].css', {
                allChunks: true
            }),
            new CommonsChunkPlugin({
                name: 'common',
                minChunks: Infinity
            }),
        ]);
    module.exports.devtool = false;
} else {
    module.exports.devtool = 'eval-source-map';
    module.exports.plugins = (module.exports.plugins || [])
        .concat([
            new ExtractTextPlugin('[name].css', {
                allChunks: true
            }),
            new webpack.HotModuleReplacementPlugin(),
            new webpack.NoErrorsPlugin(),
        ])
}
